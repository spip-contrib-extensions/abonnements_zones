<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-abozones?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

return [
    // A
    'abozones_description' => 'Este plugin permite de vincular zonas restringidas  del plugin Acceso Restringido (Accès Restreint ) a ofertas de inscripción. Todos los nuevos inscritos  a estas ofertas serán entonces automaticamente añadidos a las zonas e inversamente cuando se desactive la inscripción.',
    # MODIF
    'abozones_nom' => 'Inscripciones a zonas restringidas',
    'abozones_slogan' => 'Vincular zonas de ofertas de inscripción a zonas restringidas.',
];
