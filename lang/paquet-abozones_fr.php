<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/abonnements_zones.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

return [
    // A
    'abozones_description' => 'Ce plugin permet de lier des zones restreintes du plugin Accès Restreint à des offres d’abonnement. Tout nouvel abonné à ces offres sera alors automatiquement ajouté aux zones, et inversement lors de la désactivation de l’abonnement.',
    'abozones_nom' => 'Abonnements à des zones restreintes',
    'abozones_slogan' => 'Lier des offres d’abonnement à des zones restreintes',
];
