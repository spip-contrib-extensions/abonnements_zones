<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\Set\SpipLevelSetList;
use SpipLeague\Component\Rector\Set\SpipSetList;

return RectorConfig::configure()
	->withPaths([__DIR__, __DIR__ . '/lang'])
	->withSkip([__DIR__ . '/vendor'])
	->withSets([SpipSetList::SPIP_41, SpipLevelSetList::UP_TO_SPIP_41]);
